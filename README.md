## Aplicacion del clima - NodeJs

Aplicacion de consola que consume la api de OpenWeatherMap, simplemente se debe pasar la ciudad que se deses buscar

Se realizan peticiones a traves de Axios

Ejecutar el comando

```
npm install
```

**Ejemplo**

```
node app -d "New York"
```
